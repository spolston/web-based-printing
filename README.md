# Web Printing App

This is a simple web based app to allow students at the Sally McDonnell Barksdale Honors College to print from their laptops without having to install any drivers or printers. Using a web app allows us to wrap it a middleware to enforce that only our students are utilizing the service.  
#### Why?
1.       I can restrict the printing ability to just Honors students with the login
2.       No one has to install anything to print (which also means that I don’t have to help anyone install anything)
3.       Students can print from their laptops
4.       This started a framework that could allow the functionality to other printers in the Honors College
5.       This gave me an opportunity to write such a framework and learn new things


## Setup
#### Requisites

Composer:
* "slim/slim": "3.5.0",
* "twig/twig": "1.24",
* "adldap2/adldap2": "6.1",
* "slim/twig-view": "2.1",
* "slim/csrf": "0.7.0"
* "monolog/monolog": "1.21"

[Foundation](http://foundation.zurb.com/) for general CSS  
[Dropzone.js](http://www.dropzonejs.com/) for file upload  
[Font Awesome](http://fontawesome.io/) for icons  

Also requires [CUPS](https://www.cups.org/) to be installed to handle printing and [Libre Office](https://www.libreoffice.org/) for file conversion to PDF.

#### Config

_ldap-config.php_ and _keys-config.php_ are outside of the public directory, and contain the following vars
```
$encrypt_method
$secret_key
$secret_iv

$ldapConfig->setDomainControllers(['', '']);
$ldapConfig->setBaseDn('');
$ldapConfig->setAdminUsername('');
$ldapConfig->setAdminPassword('');
$ldapConfig->setAdminAccountSuffix('');
$ldapConfig->setAccountSuffix('');

```

#### Print

Set up the printer with Cups. Be sure to update the printer name in the *print_file* function and test it from the command line.
