<?php

date_default_timezone_set('America/Chicago');
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Psr7Middlewares\Middleware\TrailingSlash;

// Load in external files
require 'vendor/autoload.php';
require '../ldap-config.php';
require '../keys-config.php';

// Instantiate App
$app = new \Slim\App;

// Set up Active Directory Connection
$ad = new \Adldap\Adldap();
$ldapProvider = new \Adldap\Connections\Provider($ldapConfig);
$ad->addProvider('default', $ldapProvider);


/*
*
* Vars
*
*/

$upload_dir = "assets/uploads/";
$tehURL = "https://print.honors.olemiss.edu";

/*
*
* Containers
*
*/

$container = $app->getContainer();

// Twig Templating
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('assets/templates', [
        // 'cache' => 'assets/cache'
    ]);
    $view->addExtension(new \Slim\Views\TwigExtension(
        $container['router'],
        $container['request']->getUri()
    ));

    return $view;
};

// Logging

// This was a working Logging mechanism
$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
    $logger->pushHandler($file_handler);
    return $logger;
};

//Trying new logging cause the old one doesn't work in functions?


/*
*
* Middleware
*
*/

// Sessions
session_start();

// CSRF
$app->add(new \Slim\Csrf\Guard);

// IP Address
$checkProxyHeaders = true;
$trustedProxies = ['127.0.0.1']; // Note: Never trust the IP address for security processes!
$app->add(new RKA\Middleware\IpAddress($checkProxyHeaders, $trustedProxies));

// Remove Trailing Slash
$app->add(new TrailingSlash(false));

#
#
# Functions
#
#

$check_session = function ($request, $response, $next) {
  if ( count($_SESSION) == 2 ){
    if ( array_key_exists('scotty', $_SESSION) ){
      $current_time = time();
      $arr = json_decode( decrypt_session( $_SESSION['scotty'] ), true );
      $delta = $current_time - $arr['lastused'];
      if ( $delta > 15 * 60 ) {
        // Redirect to Login Page
        session_destroy();
        session_start();
        $response = $response->withStatus(302)->withHeader('Location', '/login');
        $response = $next($request, $response);
        return $response;
      } else {
        update_session_data();
        $response = $next($request, $response);
        return $response;
      }
    }
  } else {
    session_destroy();
    session_start();
    $response = $response->withStatus(302)->withHeader('Location', '/login');
    $response = $next($request, $response);
    return $response;
  }
};

$test_session = function ($request, $response, $next) {
  $response->getBody()->write($_SESSION['scotty']);
  $response = $next($request, $response);
  return $response;
};

function encrypt_session($string) {
  global $encrypt_method;
  global $key;
  global $iv;
  $output = false;
  $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
  $output = base64_encode($output);
  return $output;
};

function decrypt_session($string) {
  global $encrypt_method;
  global $key;
  global $iv;
  $output = false;
  $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
  return $output;
};

function create_session_data($webid) {
  $arr = ['username' => $webid, 'logintime' => time(), 'lastused' => time()];
  $_SESSION['scotty'] = encrypt_session(json_encode($arr));
};

function update_session_data() {
  $arr = json_decode(decrypt_session($_SESSION['scotty']), true);
  $arr['lastused'] = time();
  $_SESSION['scotty'] = encrypt_session(json_encode($arr));
};

function session_add_file( $file_path ) {
  $arr = json_decode(decrypt_session($_SESSION['scotty']), true);
  $arr["filepath"] = $file_path;
  $_SESSION['scotty'] = encrypt_session(json_encode($arr));
};

function session_get_file() {
  $arr = json_decode(decrypt_session($_SESSION['scotty']), true);
  return (isset($arr['filepath']) ? $arr['filepath'] : false);
};

function session_get_user() {
  $arr = json_decode(decrypt_session($_SESSION['scotty']), true);
  return (isset($arr['username']) ? $arr['username'] : false);
};

function print_file($file, $printer) {
  // $file is whole path to the file.
  $printer_list = array(
    "lobby" => "Section1",
    "dungeon" => "Section2",
    "lab" => "LabPrinter1",
  );
  $printer = strtolower($printer);
  if ( file_exists($file) ) {
    $err_str = system(escapeshellcmd("lpr -P " . $printer_list[$printer] ." -o sides=two-sided-long-edge -o media=Letter -T " . escapeshellarg($file)." ".escapeshellarg($file)), $err_ret);
    if (isset($err_ret) && !empty($err_ret)){
      return $err_str;
    } else {
      return true;
      // return $printer_list[$printer];
    }
  } else {
    return "File does not exist";
  }
};

function convert_file($file) {
  //$file is full path to file
  if ( file_exists($file) ) {
    $parent_dir = dirname($file);
    $cmd = "/opt/libreoffice6.2/program/soffice --headless --convert-to pdf --outdir \"" . $parent_dir . "\" " . escapeshellarg($file);
    $output = exec(escapeshellcmd($cmd), $err_ret);
    if (strpos($output, "Error") === false ){
      $new_name = split("\.", $file);
      return $new_name[0] . ".pdf";
      // return $file;
    } else {
      // return true;
      return $output;
    }
  } else {
    // return "File does not exist";
    return false;
  }
};


#
#
#  Routes
#
#


$app->get('/', function($request, $response) {
  $csrf_name = $request->getAttribute('csrf_name');
  $csrf_value = $request->getAttribute('csrf_value');
  // $tehURL = $request->getUri()->getBaseUrl();
  global $tehURL;
  $options = [ 'title' => 'Print Web App', 'printer' => $printer, 'csrf_name' => $csrf_name, 'csrf_value' => $csrf_value, 'tehURL' => $tehURL ];
  return $this->view->render($response, 'upload.twig', $options);
  // print "Hello";
})->add($check_session);

$app->get('/p/{printer}', function($request, $response) {
  $csrf_name = $request->getAttribute('csrf_name');
  $csrf_value = $request->getAttribute('csrf_value');
  $printer = $request->getAttribute('printer');
  // $tehURL = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
  // $tehURL = $request->getUri()->getBaseUrl();
  global $tehURL;
  $options = [ 'title' => 'Print Web App', 'printer' => $printer, 'csrf_name' => $csrf_name, 'csrf_value' => $csrf_value, 'tehURL' => $tehURL ];
  return $this->view->render($response, 'upload.twig', $options);
  // print "Hello";
})->add($check_session);

$app->get('/lobby', function($request, $response) {
  $csrf_name = $request->getAttribute('csrf_name');
  $csrf_value = $request->getAttribute('csrf_value');
  $printer = "lobby";
  // $tehURL = $request->getUri()->getBaseUrl();
  global $tehURL;
  $options = [ 'title' => 'Print Web App', 'printer' => $printer, 'csrf_name' => $csrf_name, 'csrf_value' => $csrf_value, 'tehURL' => $tehURL ];
  return $this->view->render($response, 'upload.twig', $options);
})->add($check_session);

$app->get('/dungeon', function($request, $response) {
  $csrf_name = $request->getAttribute('csrf_name');
  $csrf_value = $request->getAttribute('csrf_value');
  $printer = "dungeon";
  // $tehURL = $request->getUri()->getBaseUrl();
  global $tehURL;
  $options = [ 'title' => 'Print Web App', 'printer' => $printer, 'csrf_name' => $csrf_name, 'csrf_value' => $csrf_value, 'tehURL' => $tehURL ];
  return $this->view->render($response, 'upload.twig', $options);
})->add($check_session);

$app->get('/lab', function($request, $response) {
  $csrf_name = $request->getAttribute('csrf_name');
  $csrf_value = $request->getAttribute('csrf_value');
  $printer = "lab";
  // $tehURL = $request->getUri()->getBaseUrl();
  global $tehURL;
  $options = [ 'title' => 'Print Web App', 'printer' => $printer, 'csrf_name' => $csrf_name, 'csrf_value' => $csrf_value, 'tehURL' => $tehURL ];
  return $this->view->render($response, 'upload.twig', $options);
})->add($check_session);

$app->get('/login', function($request, $response) {
  // global $session;
  // $session->set('epoch', time() );
  $csrf_name = $request->getAttribute('csrf_name');
  $csrf_value = $request->getAttribute('csrf_value');
  return $this->view->render($response, 'login.twig', ['csrf_name' => $csrf_name, 'csrf_value' => $csrf_value]);
});

$app->post('/login', function($request, $response) {
  if ($request->isXhr()) {
    global $ldapProvider;
    // $arr = $request->getParsedBody();
    extract($request->getParsedBody());
    try {
      if ($ldapProvider->auth()->attempt($u, $p)){
        create_session_data($u);
        $this->logger->addInfo("$u successfully logged in");
        return 'Success!';
      } else {
        print "Wrong Username and/or Password";
      }
    } catch (\Adldap\Exceptions\Auth\UsernameRequiredException $e) {
    // The user didn't supply a username.
      print "Username/Password Required";
    } catch (\Adldap\Exceptions\Auth\PasswordRequiredException $e) {
    // The user didn't supply a password.
      print "Password/Password Required";
    }
  }
});

$app->post('/upload', function($request, $response) {
  $file = $request->getUploadedFiles();
  $filetype = $file["file"]->getClientMediaType();
  $filename = $file["file"]->getClientFilename();
  $time = time();
  $csrf_name = $request->getAttribute('csrf_name');
  $csrf_value = $request->getAttribute('csrf_value');
  $user = session_get_user();
  $ok_files = array(
                "application/pdf",
                "application/msword",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
              );
  if (in_array($filetype, $ok_files)) {
    if ( mkdir("assets/uploads/$time", 0755) ) {
      try {
        $filename = preg_replace("/[^a-zA-Z0-9\.]+/", "", $filename);
        $file_path = "assets/uploads/$time/$filename";
        $file["file"]->moveTo($file_path);
        if ( in_array($filetype, array("application/pdf")) ){
          // Start print action
          session_add_file($file_path);
          $this->logger->addInfo("$user uploaded $file_path");
          // print "Ready to Print";
          return json_encode(['csrf_name' => $csrf_name, 'csrf_value' => $csrf_value]);
        } else {
          $res = convert_file($file_path);
          if ( $res == false){
            print "Could not find file";
            $this->logger->addInfo("$user could not convert $file_path");
          } else {
            // Start print action
            $file_path = $res;
            session_add_file($file_path);
            $this->logger->addInfo("$user uploaded and converted $file_path");
            // print "Ready to Print";
            return json_encode(['csrf_name' => $csrf_name, 'csrf_value' => $csrf_value]);
          }
        }
      } catch (Exception $e) {
        print $e;
        $this->logger->addInfo("something superbad happened");
        $this->logger->addInfo($e);
      }
    } else {
      print "Failed to upload file";
      $this->logger->addInfo("$user FAILED to upload file");
    }
  } else {
    print "Wrong file type";
    $this->logger->addInfo("$user WRONG file type");
  }
})->add($check_session);

$app->post('/print-me', function($request, $response) {
  $file_path = session_get_file();
  $printer = $request->getParsedBody()['print_name'];
  if ( !isset($printer) ){
    $printer = "Scotty";
  }
  if ($file_path != false) {
    $res = print_file($file_path, $printer);
    if ( $res ) {
      $user = session_get_user();
      $this->logger->addInfo("$user printed $file_path");
      print "File Printed";
    } else {
      print $res;
      print "Something not returned from print_file";
    }
  } else {
    print "Problem retrieving file";
  }
})->add($check_session);

$app->post('/update-t', function($request, $response) {
  $csrf_name = $request->getAttribute('csrf_name');
  $csrf_value = $request->getAttribute('csrf_value');
  return json_encode(['csrf_name' => $csrf_name, 'csrf_value' => $csrf_value]);
})->add($check_session);

$app->get('/logout', function($request, $response) {
  session_destroy();
  return $response->withStatus(302)->withHeader('Location', '/');
})->add($check_session);

$app->get('/cleanup', function($request, $response) {
  global $upload_dir;
  $source_ip = $request->getAttribute('ip_address');
  if ( $source_ip === $_SERVER['SERVER_ADDR'] ){
    $max_age = time() - (24 * 60 * 60);
    foreach ( scandir($upload_dir) as $dirname ){
      if ( preg_match("/\./", $dirname) == 0){
        if ( (int) $dirname < $max_age ){
          foreach (scandir($upload_dir."/".$dirname) as $file){
            if ( preg_match("/^\./", $file) == 0){
              if ( unlink($upload_dir."/".$dirname."/".$file) ){
                print "Deleted: ". $file . "\n";
              } else {
                print "Errored: ". $file . "\n";
              }
            }
          }
          if ( rmdir($upload_dir."/".$dirname) ){
            print "Deleted: ";
          } else {
            print "Errored: ";
          }
          print $dirname . "\n";
        }
      }
    }
  } else {
    print $_SERVER['SERVER_ADDR'];
  }
});


$app->get('/scotty', function($request, $response) {
  return "scotty";
});

$app->run();
